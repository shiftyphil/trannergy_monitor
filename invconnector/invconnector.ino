/* vim:set syn=c ts=4 sw=4 noet: */
#include <stdio.h>
#include <Dhcp.h>
#include <Dns.h>
#include <Ethernet.h>
#include <EthernetClient.h>
#include <EthernetServer.h>
#include <EthernetUdp.h>
#include <SoftwareSerial.h>
#include <SPI.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include <avr/pgmspace.h>

#define VERSION "1.2.1"

/* These are for debugging purposes only */
#define FORCE_DHCP    0
#define FORCE_STATIC  0
#define DEBUG_NTP     0 /* 222 bytes */
#define DEBUG_CRC     0

#define CLI_CONFIG    1 /* 5526 bytes */
#define SHOW_MEM      1 /*  194 bytes */
#define SHOW_STATUS   0 /*  640 bytes */
/* telnet server allows configuration/status via telnet in addition to serial. */
#define TELNET_SERVER 1 /* 1344 bytes */
#define DEBUGGING     0 /*  590 bytes */

/* Convert seconds to milliseconds */
#define msecs(SECS) ((SECS) * 1000lu)

/* Suppress no ground fault for 2 minutes */
#define SUPPRESS_NO_FAULT (2 * 60)
#define ONE_HOUR msecs(60 * 60)
#define PVOUTPUT_HOST "pvoutput.org"
#define PVOUTPUT_PORT 80
#define UPLOAD_INTERVAL (5 * 60)
#define LOG_HOST "hippiesparx.com.au"
#define LOG_PORT 80

#define NTP_HOST_1 "hippiesparx.com.au"
#define NTP_HOST_2 "oceania.pool.ntp.org"

#define GROUND_FAULT_PIN 8
#define GROUND_FAULT_BUTTON 9
#define LED_PIN 13

/* We store the last known ground fault setting
 * in the last byte of eeprom
 */
#define GROUND_FAULT_ADDR ((unsigned char *)1023)

#if 0
	#define PVOUTPUT_HOST "stout."
	#define PVOUTPUT_PORT 8000
	#define UPLOAD_INTERVAL (2 * 60)
	#define LOG_HOST "stout."
	#define LOG_PORT 8081
#endif

// packet indexes
const int P_CCODE = 6;
const int P_FCODE = 7;
const int P_DATALEN = 8;
const int P_DATA = 9;
// Just make these constants
const byte INV_SRC = 0x01;
const byte INV_DST = 0x02;

const byte CC_REG = 0x10;
const byte FC_OLQ = 0x00;
const byte FC_OLQR = 0x80;
const byte FC_REG = 0x01;
const byte FC_REGACK = 0x81;
const byte FC_REREG = 0x04;

const byte CC_READ = 0x11;
const byte FC_RODATA = 0x02;
const byte FC_DATA = 0x82;

const byte INV_ACK = 0x06;

/* time (in ms) that we expect the next PV event */
unsigned long next_pv_event;
/* time (in ms) of next pvoutput update */
unsigned long next_pvupdate;
/* time (in ms) of last successful pvoutput or hippiesparx update */
unsigned long last_pvupdate;
/* time (in ms) at which to send an NTP request if time is not set */
unsigned long next_ntp;
/* time (in ms) to maybe signal the ground error alarm */
unsigned long next_ground_err_check;
/* if the network is initialised */
byte network_initialised;
byte ground_error;
byte send_immediately;

/* inverter serial number */
byte pv_serial[17];

SoftwareSerial invserial(2,3);
HardwareSerial &logserial = Serial;

/* for debugging/cli */
static byte debug;
static byte cmdpos;

/* Help with storing format strings in flash */
#define xputs(MSG) puts_P(PSTR(MSG))
#define xprintf(FMT,ARGS...) printf_P(PSTR(FMT),ARGS)
#define xfprintf(FH, FMT,ARGS...) fprintf_P(FH, PSTR(FMT),ARGS)
#define xfputs(FH, MSG) do { fputs_P(PSTR(MSG), FH); fputc('\n', FH); } while (0)
#if DEBUGGING
#define xdputs(MSG) do { if (debug && cmdpos == 0) xputs(MSG); } while (0)
#else
#define xdputs(MSG)
#endif

#define EEPROM_MAGIC 0xc1
struct config {
	byte magic; /* magic EEPROM_MAGIC means data is valid */
	byte dhcp; /* 1 means use dhcp */
	signed char tzoff; /* timezone offset in hours. Thus -12 to +12 */
	byte mac[6];
	byte ip[4]; /* static ip address if not dhcp */
	byte gw[4]; /* gateway ip address if not dhcp */
	/* Note: Null terminated. */
	char apikey[41];
	char sysid[9];
	/* If you change the fields above here, increment EEPROM_MAGIC */
} econfig;

struct packet {
	byte datalen;
	byte data[P_DATA + 256 + 2];
	int packetlen() { return datalen + P_DATA + 2; }
	byte *payload(int offset) { return data + P_DATA + offset; }
};

enum {
	IS_NOT_REG,		// Not registered - send REREG, OLQ
	IS_SENT_REREG,	// Sent REREG - wait 1s */
	IS_SENT_OLQ,	// Sent OLQ - wait up to 2s for OLQR
	IS_SENT_REG,	// Sent REG - wait up to 2s for ACK
	IS_SENT_QUERY,	// Sent data query
	IS_QUERY_WAIT,	// Got data, wait until next fetch
};

static byte pvstate = IS_NOT_REG;
static byte pv_response_expected;

/* We only ever send or receive one packet at a time */
struct packet pkt;

#if TELNET_SERVER && CLI_CONFIG
// Telnet server for config
static EthernetServer ts(23);
static EthernetClient tc;
static byte telnet_negotiated;

static FILE telnetfh;

static int telnet_putter(char c, FILE *stream)
{
	if (c == '\n') {
		tc.write('\r');
	}
	tc.write(c);
	return 0 ;
}
#endif

/* Allow stdio to logserial */
static FILE serialfh;

static int serial_putter(char c, FILE *stream)
{
	if (c == '\n') {
		logserial.write('\r');
	}
	logserial.write(c);
	return 0 ;
}

/* Allow stdio to pvoutput client connection */
static EthernetClient pvc;

/* stream buffer and putter for sending HTTP requests.
 * Use a buffer here to prevent many 1 byte network packets
 */
static FILE pvcfh;
static byte pvc_n;
static byte pvc_buf[64];

static void pvc_flush(void)
{
	if (pvc_n) {
		pvc.write(pvc_buf, pvc_n);
		pvc_n = 0;
	}
}

static int pvc_putter(char c, FILE *stream)
{
	/* Send to both pvc and log serial for diagnostics */
	logserial.write(c);

	pvc_buf[pvc_n++] = c;
	if (pvc_n >= sizeof(pvc_buf)) {
		pvc_flush();
	}

	return 0 ;
}

static void u32ton(byte *b, unsigned long l)
{
	/* We know the l is in little endian order, so convert to big endian */
	byte *lb = (byte *)&l;
	b[0] = lb[3];
	b[1] = lb[2];
	b[2] = lb[1];
	b[3] = lb[0];
}

static unsigned long ntou32(const byte *b)
{
	byte i;
	unsigned long l = 0;
	for (i = 0; i < 4; i++) {
		l = (l << 8) + b[i];
	}
	return l;
}

static unsigned ntou16(const byte *b)
{
	byte i;
	unsigned l = 0;
	for (i = 0; i < 2; i++) {
		l = (l << 8) + b[i];
	}
	return l;
}

#if SHOW_MEM
int get_free_memory()
{
	extern int __bss_end;
	extern void *__brkval;

	int free_memory;

	if((int)__brkval == 0)
		free_memory = ((int)&free_memory) - ((int)&__bss_end);
	else
		free_memory = ((int)&free_memory) - ((int)__brkval);

	return free_memory;
}
#endif

/* @ms */
const char *ms()
{
	static char buf[10];

	unsigned long now = millis();
	snprintf(buf, sizeof(buf), "%d.%02d", now / 1000, (now / 10) % 100);

	return buf;
}

unsigned calc_cksum(struct packet *p)
{
	byte i;
	unsigned cksum = 0;
	for (i = 0; i < P_DATA + p->datalen; i++) {
		cksum += pkt.data[i];
	}
	return cksum;
}

/* CRC-CCITT: x^16+x^12+x^5+1 */
static const unsigned short crc16_tab[256] PROGMEM = {
	0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
	0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
	0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
	0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
	0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
	0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
	0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
	0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
	0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
	0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
	0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
	0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
	0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
	0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
	0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
	0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
	0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
	0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
	0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
	0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
	0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
	0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
	0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
	0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
	0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
	0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
	0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
	0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
	0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
	0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
	0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
	0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0,
};

static unsigned short crc16(unsigned short crc, const unsigned char *message, int len)
{
#if DEBUG_CRC
	xprintf("%s", "crc16 over: ");
#endif
	while (len--) {
#if DEBUG_CRC
		xprintf("%02x ", *message);
#endif
		unsigned char n = ((crc>>8) ^ *message++) & 0xFF;
		crc = pgm_read_word(crc16_tab + n) ^ (crc << 8);
	}
#if DEBUG_CRC
	xprintf(" => %04x\n", crc);
#endif

	return crc;
}

// makePacket w/ data
static void sendPacket(byte source, byte destination, byte controlCode, byte functionCode, byte len, byte data[]) {
	// 0-1:					header
	// 2-3:					source
	// 4-5:					destination
	// 6:						control code
	// 7:						function code
	// 8:						data length
	// 9-(n-2):			data
	// (n-2)-(n-1): checksum
	int i;

	pkt.datalen = len;
	pkt.data[0] = 0xAA;
	pkt.data[1] = 0x55;
	pkt.data[2] = source;
	pkt.data[3] = 0x00;
	pkt.data[4] = 0x00;
	pkt.data[5] = destination;
	pkt.data[6] = controlCode;
	pkt.data[7] = functionCode;
	pkt.data[8] = len;
	memcpy(&pkt.data[P_DATA], data, len);

	unsigned cksum = calc_cksum(&pkt);

	byte *cpos = &pkt.data[P_DATA + len];

	cpos[0] = (byte)(cksum >> 8);
	cpos[1] = (byte)(cksum & 0xff);

	//Serial.flush();
	invserial.write(pkt.data, pkt.packetlen());
}

static byte serial_read(byte *ok)
{
	if (!invserial.available()) {
		/* Wait up to 5ms for a char */
		delay(5);
		if (!invserial.available()) {
			*ok = 0;
			return 0;
		}
	}
	return invserial.read();
}

static int readPacket() {
	byte ok = 1;
	byte i;

	// Read with timeout
	for (i = 0; i < P_DATA; i++) {
		pkt.data[i] = serial_read(&ok);
	}
	if (!ok || pkt.data[0] != 0xaa  || pkt.data[1] != 0x55) {
		xputs("failed header check");
		return 0;
	}

	pkt.datalen = pkt.data[P_DATALEN];

	for (i = P_DATA; i < pkt.packetlen(); i++) {
		pkt.data[i] = serial_read(&ok);
	}

	if (!ok) {
		xputs("failed to read data");
		return 0;
	}

	byte *cpos = &pkt.data[P_DATA + pkt.datalen];

	unsigned cksum = (cpos[0] << 8) + cpos[1];

	if (cksum != calc_cksum(&pkt)) {
		xprintf("bad cksum: got %04x, exp %04x", cksum, calc_cksum(&pkt));
		log_packet(&pkt);
		return 0;
	}
#if DEBUGGING
	if (debug && !cmdpos) {
		log_packet(&pkt);
	}
#endif
	return 1;
}

/* Date/Time */

struct tm {
	byte tm_sec;
	byte tm_min;
	byte tm_hour;
	byte tm_wday; /* 0 = sunday */
	byte tm_day; /* 1 based */
	byte tm_mon; /* 1 based */
	int tm_year; /* e.g. 2012 */
};

// leap year calulator expects year argument as years offset from 1970
#define LEAP_YEAR(Y)     ( ((1970+Y)>0) && !((1970+Y)%4) && ( ((1970+Y)%100) || !((1970+Y)%400) ) )

static  const byte monthDays[]={31,28,31,30,31,30,31,31,30,31,30,31}; // API starts months from 1, this array starts from 0
static const char *downames[] = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};

static void gmtime(unsigned long time, struct tm &tm)
{
	byte year;
	byte month, monthLength;
	unsigned days;

	tm.tm_sec = time % 60;
	time /= 60; // now it is minutes
	tm.tm_min = time % 60;
	time /= 60; // now it is hours
	tm.tm_hour = time % 24;
	time /= 24; // now it is days
	tm.tm_wday = ((time + 4) % 7);	// Sunday is day 0 

	year = 0;
	days = 0;
	while((days += (LEAP_YEAR(year) ? 366 : 365)) <= time) {
		year++;
	}
	tm.tm_year = year + 1970; // year is offset from 1970 

	days -= LEAP_YEAR(year) ? 366 : 365;
	time	-= days; // now it is days in this year, starting at 0

	monthLength=0;
	for (month=0; month<12; month++) {
		if (month==1 && LEAP_YEAR(year)) {
			/* February */
			monthLength=29;
		}
		else {
			monthLength = monthDays[month];
		}
		if (time < monthLength) {
			break;
		}
		time -= monthLength;
	}
	tm.tm_mon = month + 1;
	tm.tm_day = time + 1;			// day of month
}

static void saveconfig()
{
	econfig.magic = EEPROM_MAGIC;
	eeprom_write_block(&econfig, NULL, sizeof(econfig));
}

byte ntpbuf[48];
EthernetUDP ntpc;
unsigned long epoch;
unsigned long sync_off;
byte ntpwhich = 0;
static const char *ntp_hosts[2] = {
	NTP_HOST_1,
	NTP_HOST_2,
};

/**
 * Return a time at most 'interval' milliseconds
 * in the future, which aligns on a boundary of 'interval' milliseconds.
 * 
 * For example, if it is currently 11:13:07 and interval is (15 * 1000) milliseconds,
 * this will return 11:13:15, as milliseconds since uptime.
 */
unsigned long next_interval(unsigned long now, unsigned long interval)
{
	return now + interval - (now + sync_off) % interval;
}

static unsigned long sendNTPpacket(EthernetUDP &c)
{
#if DEBUG_NTP
	xprintf("sending NTP request to %s\n", ntp_hosts[ntpwhich]);
#endif
	// set all bytes in the buffer to 0
	memset(ntpbuf, 0, sizeof(ntpbuf));

	// Initialize values needed to form NTP request
	// (see URL above for details on the packets)
	ntpbuf[0] = 0b11100011;		// LI, Version, Mode
	ntpbuf[1] = 0;		 // Stratum, or type of clock
	ntpbuf[2] = 6;		 // Polling Interval
	ntpbuf[3] = 0xEC;	 // Peer Clock Precision
	// 8 bytes of zero for Root Delay & Root Dispersion
	ntpbuf[12]	= 49;
	ntpbuf[13]	= 0x4E;
	ntpbuf[14]	= 49;
	ntpbuf[15]	= 52;

	c.beginPacket(ntp_hosts[ntpwhich], 123);
	c.write(ntpbuf, sizeof(ntpbuf));
	c.endPacket();

	ntpwhich = !ntpwhich;
}

static void showtime(FILE *fh)
{
	if (epoch) {
		unsigned long now = epoch + millis() / 1000;
		struct tm tm;
		gmtime(now + econfig.tzoff * 3600L, tm);
		xfprintf(fh, "%02d:%02d:%02d %s %02d-%02d-%04d\n",
			tm.tm_hour, tm.tm_min, tm.tm_sec, downames[tm.tm_wday], tm.tm_day, tm.tm_mon, tm.tm_year);
	}
	else {
		xfputs(fh, "?");
	}
}

static void parseNTPpacket(EthernetUDP &ntpc)
{
	ntpc.read(ntpbuf, sizeof(ntpbuf));

#if DEBUG_NTP
	xprintf("ntp response: 4 bytes: %s\n", fmt_bytes(ntpbuf + 40, 4, ' ', "%02x"));
#endif
	if (ntpbuf[40] < 0xd0) {
		xputs("bad ntp response");
		return;
	}

	epoch = ntou32(ntpbuf + 40) - 2208988800UL - millis() / 1000;

	/* Calculate the synchronisation offset now.
	 * Use mod 3600 to avoid overflow
	 */
	sync_off = (epoch % 3600) * 1000;

	showtime(&serialfh);
}

void (*reset)() = 0;

void setup() {
	wdt_disable();
	invserial.begin(9600);
	logserial.begin(115200);

	/* Set up stdout on logserial */
	fdev_setup_stream(&serialfh, serial_putter, NULL, _FDEV_SETUP_WRITE);
	stdout = &serialfh;

#if SHOW_MEM
	xprintf("\nFree Mem: %d\n", get_free_memory());
#endif

	pinMode(LED_PIN, OUTPUT);
	digitalWrite(LED_PIN, LOW);

	pinMode(GROUND_FAULT_PIN, OUTPUT);

	/* Turn on pull-up */
	pinMode(GROUND_FAULT_BUTTON, INPUT);
	digitalWrite(GROUND_FAULT_BUTTON, HIGH);

	eeprom_read_block(&econfig, NULL, sizeof(econfig));
	if (econfig.magic != EEPROM_MAGIC) {
		memset(&econfig, 0, sizeof(econfig));
		/* No need to set zero fields below */
		memcpy(&econfig.mac, "\xde\xad\xbe\xef\xfe\xed", 6);
		econfig.ip[0] = 192;
		econfig.ip[1] = 168;
		/*econfig.ip[2] = 0;*/
		econfig.ip[3] = 2;
		econfig.gw[0] = 192;
		econfig.gw[1] = 168;
		/*econfig.gw[2] = 0;*/
		econfig.gw[3] = 1;
		/*econfig.dhcp = 0;*/
		econfig.tzoff = 10;
		econfig.sysid[0] = '?';
		econfig.apikey[0] = '?';
	}
	else {
		/* Read the last known ground error */
		ground_error = !!eeprom_read_byte(GROUND_FAULT_ADDR);
	}

	wdt_enable(WDTO_8S);
}

static int parse_mac(const char *str, byte mac[6])
{
	byte tmp[6];
	byte i;

	for (i = 0; i < 6; i++) {
		char *end;
		int octet = strtol(str, &end, 16);
		if (end == str || octet < 0 || octet > 255) {
			return 0;
		}
		tmp[i] = octet;
		if (!*end) {
			break;
		}
		if (*end != ':') {
			return 0;
		}
		str = end + 1;
	}
	if (i != 5) {
		return 0;
	}
	memcpy(mac, tmp, 6);
	return 1;
}

static int parse_ip(const char *str, byte ip[4])
{
	byte tmp[4];
	byte i;

	for (i = 0; i < 4; i++) {
		char *end;
		int octet = strtol(str, &end, 10);
		if (end == str || octet < 0 || octet > 255) {
			return 0;
		}
		tmp[i] = octet;
		if (!*end) {
			break;
		}
		if (*end != '.') {
			return 0;
		}
		str = end + 1;
	}
	if (i != 3) {
		return 0;
	}
	memcpy(ip, tmp, 4);
	return 1;
}

#if CLI_CONFIG
static char cmdbuf[64];

static void showconfig(FILE *fh)
{
	xfputs(fh, "--- config ---");
	xfprintf(fh, "   mac: %s\n", fmt_bytes(econfig.mac, 6, ':', "%02x"));
	xfprintf(fh, "    ip: %s\n", (econfig.dhcp) ? "<dhcp>" : fmt_bytes(econfig.ip, 4, '.', "%d"));
	if (!econfig.dhcp) {
		xfprintf(fh, "    gw: %s\n", fmt_bytes((byte *)&econfig.gw, 4, '.', "%d"));
	}
	xfprintf(fh, " sysid: %s\n", econfig.sysid);
	xfprintf(fh, "apikey: %s\n", econfig.apikey);
	xfprintf(fh, " tzoff: %+d\n", econfig.tzoff);
	xfputs(fh, "--- status ---");
	if (network_initialised) {
		unsigned long ip = Ethernet.localIP();
		xfprintf(fh, "    ip: %s\n", fmt_bytes((byte *)&ip, 4, '.', "%d"));
		ip = Ethernet.gatewayIP();
		xfprintf(fh, "    gw: %s\n", fmt_bytes((byte *)&ip, 4, '.', "%d"));
		ip = Ethernet.dnsServerIP();
		xfprintf(fh, "   dns: %s\n", fmt_bytes((byte *)&ip, 4, '.', "%d"));
		xfprintf(fh, "  time: ", 0);
		showtime(fh);
	}
	struct tm tm;
	unsigned long now = millis();
	gmtime(now / 1000, tm);
	xfprintf(fh, "uptime: %02d:%02d:%02d\n", tm.tm_hour, tm.tm_min, tm.tm_sec);
	xfprintf(fh, " comms: %s\n", (pvstate <= IS_SENT_REG) ? "not-registered" : "inverter-ok");
	if (pvstate >= IS_SENT_REG) {
		xfprintf(fh, " serno: %s\n", pv_serial);
	}
	if (pvstate == IS_QUERY_WAIT) {
		unsigned long next_event = (next_pvupdate < now) ? next_pv_event : next_pvupdate;

		xfprintf(fh, "gnderr: %d\n", ground_error);
		gmtime(epoch + next_event / 1000 + econfig.tzoff * 3600L, tm);
		xfprintf(fh, "  next: in %lus at %02d:%02d:%02d\n",
			time_until(next_event), tm.tm_hour, tm.tm_min, tm.tm_sec);
	}
#if SHOW_MEM
	xfprintf(fh, "   mem: %d\n", get_free_memory());
#endif
}

static void showwelcome(FILE *fh)
{
	xfputs(fh, "\nWelcome to the Arduino Inverter Connector v" VERSION "\n\nPress ? for help/config\n");
}

static int handle_cli_char(FILE *fh, char c, int welcome)
{
	if (cmdpos == 0 && (c == '?' || c == '\n' || c == '\r')) {
		if (welcome) {
			showwelcome(fh);
		}
		xfputs(fh, "m <mac>, i <ip>|dhcp, g <gw>, s <sysid>, a <apikey>, t <tzoff>");
#if DEBUGGING
		xfputs(fh, "r = reboot, f = alarm reset, d = debug toggle");
#else
		xfputs(fh, "r = reboot, f = alarm reset");
#endif
		showconfig(fh);
		return 1;
	}
	if (cmdpos == sizeof(cmdbuf) - 1) {
		xfputs(fh, "\ncmd too long");
		cmdpos = 0;
		return 1;
	}
	if (c == '\r' || c == '\n') {
		const char *arg = "";
		fputc('\n', fh);
		cmdbuf[cmdpos] = 0;
		cmdpos = 0;
		if (cmdbuf[1] == ' ') {
			arg = &cmdbuf[2];
		}
		switch (cmdbuf[0]) {
#if DEBUGGING
			case 'd':
				debug = !debug;
				xfprintf(fh, "debug is %s\n", debug ? "on" : "off");
				return 1;
#endif

			case 'r':
				xfputs(fh, "resetting...");
				delay(500);
				reset();
				return 1;

			case 'f':
				xfputs(fh, "reset alarm for 1 hour");
				next_ground_err_check = millis() + ONE_HOUR;
				return 1;

			/* For each value, validate and update econfig */
			case 'i':
				if (*arg == 'd') {
					econfig.dhcp = 1;
					break;
				}
				if (parse_ip(arg, econfig.ip)) {
					econfig.dhcp = 0;
					break;
				}
				goto badarg;

			case 'g':
				if (econfig.dhcp == 0 && parse_ip(arg, econfig.gw)) {
					break;
				}
				goto badarg;

			case 'm':
				if (!parse_mac(arg, econfig.mac)) {
					goto badarg;
				}
				break;

			case 'a':
				if (strlen(arg) != sizeof(econfig.apikey) - 1) {
					goto badarg;
				}
				strcpy((char *)econfig.apikey, arg);
				break;

			case 's':
				if (strlen(arg) == 0 || strlen(arg) >= sizeof(econfig.sysid)) {
badarg:
					xfputs(fh, "bad arg");
					return 1;
				}
				strcpy((char *)econfig.sysid, arg);
				break;

			case 't':
				{
					char *end;
					int off = strtol(arg, &end, 10);
					if (end == arg || off < -12 || off > 12) {
						goto badarg;
					}
					econfig.tzoff = off;
				}
				break;

			default:
				xfputs(fh, "???");
				return 1;
		}
		saveconfig();
		showconfig(fh);
		return 1;
	}
	if ((cmdpos == 0 && (c >= 'a' && c <= 'z')) || cmdpos > 0) {
		if (c == 0x7f || c == '\b') {
			cmdpos--;
			fprintf(fh, "\b \b");
		}
		else {
			cmdbuf[cmdpos++] = c;
			fputc(c, fh);
		}
	}
	return 0;
}

static void check_cli()
{
	/* Note: We only support accepting a command from either serial or telnet */
	while (logserial.available()) {
		handle_cli_char(&serialfh, logserial.read(), 1);
	}
#if TELNET_SERVER
	if (network_initialised) {
		tc = ts.available();
		if (!tc.connected()) {
			telnet_negotiated = 0;
		}
		while (tc.available()) {
			int c = tc.read();
			if (c == 0xff && cmdpos == 0 && !telnet_negotiated) {
				/* force switch to char mode */
				/* IAC WONT LINEMODE IAC WILL ECHO */
				fprintf(&telnetfh, "\377\375\042\377\373\001");
				telnet_negotiated = 1;
				/* skip (simple) telnet negotiation bytes */
				while (tc.available()) {
					tc.read();
				}
				showwelcome(&telnetfh);
				continue;
			}
			handle_cli_char(&telnetfh, c, 0);
		}
	}
#endif
}
#endif

static void pvc_check_response()
{
	char c = '\n';
	xputs("Checking response");
	while (pvc.connected()) {
		if (pvc.available()) {
			c = pvc.read();
			putchar(c);
		}
		delay(1);
	}
	if (c != '\n') {
		putchar('\n');
	}
	xputs("done");
	pvc.stop();
}

static void processData(packet *p, unsigned long now)
{
	unsigned temp_10 = ntou16(p->payload(0));
	unsigned etoday_100 = ntou16(p->payload(2));
	unsigned pv1_10 = ntou16(p->payload(4));
	unsigned pv2_10 = ntou16(p->payload(6));
	unsigned pi1_10 = ntou16(p->payload(8));
	unsigned pi2_10 = ntou16(p->payload(10));

	unsigned vac_10 = ntou16(p->payload(14));
	unsigned instpower = ntou16(p->payload(18));
	unsigned long etot = 100LU * ntou16(p->payload(24));
	unsigned long errorCode = ntou32(p->payload(46));
	unsigned int errorMode = ntou16(p->payload(30));

	// permament fault or ground fault or isolation fault(can be thrown instead of ground fault)
	byte detected_ground_error = (errorMode == 0x03 || errorCode & ((1 << 10) | (1 << 18)));

	if (ground_error != detected_ground_error) {
		/* Change in status of ground error */
		/* For the first few minutes, if there is no detected ground error, use whatever
		 * we remembered in EEPROM
		 */
		if (!detected_ground_error && now < msecs(SUPPRESS_NO_FAULT)) {
#ifdef DEBUGGING
			xdputs("Ground error status lost in first 2 minutes - ignoring\n");
#endif
		}
		else {
			/* New ground error condition, so remember it */
			ground_error = detected_ground_error;
#ifdef DEBUGGING
			if (debug && cmdpos == 0) {
				xprintf("Writing ground error: %d\n", ground_error);
			}
#endif
			eeprom_write_byte(GROUND_FAULT_ADDR, ground_error);
			send_immediately = 1;
		}
	}

	struct tm tm;
	unsigned long gmsecs = epoch + now / 1000;

	gmtime(gmsecs + econfig.tzoff * 3600L, tm);

	// XXX: Note that the following is just for logging/debugging.
	//      If we run out of space, it can be trimmed/deleted

#if SHOW_STATUS
	if (epoch) {
		xprintf("time: ", 0);
		showtime(&serialfh);
	}
	xprintf("temp: %s\n", fmt_div10(temp_10));
	xprintf("Vac: %sV\n", fmt_div10(vac_10));
	xprintf("etoday: %sWh\n", fmt_div100(etoday_100));
	xprintf("etot: %luWh\n", etot);
	xprintf("instpower: %uW\n", instpower);
	xprintf("PV1: %s\n", fmt_div10(pv1_10));
	xprintf("PI1: %s\n", fmt_div10(pi1_10));
	xprintf("PV2: %s\n", fmt_div10(pv2_10));
	xprintf("PI2: %s\n", fmt_div10(pi2_10));
	xprintf("ERR: %d\n", ground_error);
#endif
	if (now >= next_pvupdate || send_immediately) {
		if (!network_initialised) {
			xputs("No network => no PVOutput");
		}
		else if (epoch == 0) {
			xputs("No NTP => no PVOutput");
		}
		else {
			next_pvupdate = next_interval(now, msecs(UPLOAD_INTERVAL));

			if (pvc.connect(PVOUTPUT_HOST, PVOUTPUT_PORT)) {
				showtime(&serialfh);
				xprintf("Connect %s:%d\n", PVOUTPUT_HOST, PVOUTPUT_PORT);
				fprintf_P(&pvcfh, PSTR("GET /service/r2/addstatus.jsp"));
				fprintf_P(&pvcfh, PSTR("?d=%04d%02d%02d&t=%02d:%02d"),
					tm.tm_year, tm.tm_mon, tm.tm_day, tm.tm_hour, tm.tm_min);
				fprintf_P(&pvcfh, PSTR("&v1=%lu&v2=%u"), etoday_100 * 10LU, instpower);
				fprintf_P(&pvcfh, PSTR("&v5=%s"), fmt_div10(temp_10));
				fprintf_P(&pvcfh, PSTR("&v6=%s"), fmt_div10(vac_10));
				fprintf_P(&pvcfh, PSTR("&v7=%d&key=%s&sid=%s\r\n\r\n"), ground_error, econfig.apikey, econfig.sysid);
				pvc_flush();

				pvc_check_response();
				last_pvupdate = now;
				send_immediately = 0;
			}
			else {
				xputs("PVOutput not up");
			}

			if (pvc.connect(LOG_HOST, LOG_PORT)) {
				/* Calculate a crc16 over the data field plus  */
				/* Store a random value in data[12..13] */
				*p->payload(12) = random(256);
				*p->payload(13) = random(256);
				/* And a fixed value in data[16..17] */
				*p->payload(16) = 0xAA;
				*p->payload(17) = 0x55;
				/* Now seconds since epoch as a network-byte-order 32 bit value */
				u32ton(p->payload(20), gmsecs);
				/* Now the 17 byte, zero-padded serial number at the end */
				memcpy(p->payload(24), pv_serial, sizeof(pv_serial));

				/* Calculate the CRC16 */
				unsigned short crc = crc16(0, p->payload(0), 24 + sizeof(pv_serial));

				showtime(&serialfh);
				xprintf("Connect %s:%d\n", LOG_HOST, LOG_PORT);
				fprintf_P(&pvcfh, PSTR("GET /invput?id=%s;v=%s"), pv_serial, VERSION);
				fprintf_P(&pvcfh, PSTR(";data=%s"), fmt_div10(temp_10));
				fprintf_P(&pvcfh, PSTR(",%s"), fmt_div10(vac_10));
				fprintf_P(&pvcfh, PSTR(",%s"), fmt_div10(pv1_10));
				fprintf_P(&pvcfh, PSTR(",%s"), fmt_div10(pi1_10));
				fprintf_P(&pvcfh, PSTR(",%s"), fmt_div10(pv2_10));
				fprintf_P(&pvcfh, PSTR(",%s"), fmt_div10(pi2_10));
				fprintf_P(&pvcfh, PSTR(",%s"), fmt_div100(etoday_100));
				fprintf_P(&pvcfh, PSTR(",%lu,%u,%u,%lu;stamp=%lu"), etot, instpower, errorMode, errorCode, gmsecs);
				/* random byte 1, crc, random byte 2 */
				fprintf_P(&pvcfh, PSTR(";x=%02x%04x%02x\r\n\r\n"), *p->payload(12), crc, *p->payload(13));
				pvc_flush();

				pvc_check_response();
				last_pvupdate = now;
				send_immediately = 0;
			}
			else {
				xputs("hippiesparx not up");
			}
		}
	}
}

#ifdef DEBUGGING
static void log_packet(packet *p)
{
	for (byte i = 0; i < p->datalen + P_DATA + 2; i++) {
		if (i == 2 || i == P_DATA || i == P_DATA + p->datalen) {
			putchar(' ');
		}
		xprintf("%02x", p->data[i]);
	}
	putchar('\n');
}
#endif

char divxbuf[20];

static const char *fmt_div10(int value)
{
	snprintf(divxbuf, sizeof(divxbuf), "%d.%d", value / 10, abs(value) % 10);
	return divxbuf;
}

static const char *fmt_div100(int value)
{
	snprintf(divxbuf, sizeof(divxbuf), "%d.%02d", value / 100, abs(value) % 100);
	return divxbuf;
}

static const char *fmt_bytes(const byte *b, byte len, char sep, const char *fmt)
{
	/* mac addr: 6 * 3 = 18, ip adddr: 4 * 4 = 16 */
	static char fmtbuf[6 * 3];

	if (len > 6) {
		len = 6;
	}

	int n = 0;
	for (byte i = 0; i < len; i++) {
		if (i) {
			fmtbuf[n++] = sep;
		}
		n += sprintf(fmtbuf + n, fmt, b[i]);
	}
	return fmtbuf;
}

static unsigned long time_since(unsigned long last)
{
	return (millis() - last) / 1000;
}

static unsigned long time_until(unsigned long next)
{
	return (next - millis()) / 1000;
}

static inline void SET_PV_EVENT(byte newstate, unsigned long timeout, byte response)
{
	pvstate = newstate;
	next_pv_event = timeout;
	pv_response_expected = response;
}

/* This loop maintains the conversation with the PV system.
 */
static void pv_loop()
{
	unsigned long now = millis();

	if (pv_response_expected && invserial.available() && readPacket()) {
		/* We expected a response and got it */
		switch (pvstate) {
			case IS_SENT_OLQ:
				if (pkt.data[P_CCODE] == CC_REG && pkt.data[P_FCODE] == FC_OLQR) {
					/* Null terminate */
					pkt.data[P_DATA + pkt.datalen] = 0;
					memcpy(pv_serial, &pkt.data[P_DATA], sizeof(pv_serial));

					xprintf("Got OLQR serial: %s\n", pv_serial);

					/* Temporarily use the last byte */
					pv_serial[16] = INV_DST;
					sendPacket(INV_SRC, 0, CC_REG, FC_REG, sizeof(pv_serial), pv_serial);
					pv_serial[16] = 0;

					/* Wait for first data response */
					SET_PV_EVENT(IS_SENT_REG, now + 2000, 1);
				}
				return;

			case IS_SENT_REG:
				if (pkt.data[P_CCODE] == CC_REG && pkt.data[P_FCODE] == FC_REGACK) {
					if (pkt.data[P_DATA] == INV_ACK) {
						xdputs("Got REG ACK, fetching data");

						sendPacket(INV_SRC, INV_DST, CC_READ, FC_RODATA, 0, NULL);
						SET_PV_EVENT(IS_SENT_QUERY, now + 2000, 1);
					}
					else {
						xdputs("Got REG NAK");
						SET_PV_EVENT(IS_NOT_REG, now + 5000, 0);
					}
				}
				return;

			case IS_SENT_QUERY:
				if (pkt.data[P_CCODE] == CC_READ && pkt.data[P_FCODE] == FC_DATA) {
					xdputs("Got some data");

					/* Note: we don't necessarily do the pv update */
					processData(&pkt, now);

					/* Wait up to 15 seconds until the next check */
					SET_PV_EVENT(IS_QUERY_WAIT, next_interval(now, 15000), 0);
				}
				return;
		}
		/* Fall through to check for timeout */
	}

	if (now < next_pv_event) {
		return;
	}

	/* PV Event timeout */
	switch (pvstate) {
		case IS_NOT_REG:
			xdputs("Not registered, sending re-reg");
			sendPacket(INV_SRC, 0, CC_REG, FC_REREG, 0, NULL);
			/* No reply expected. wait 1 second */
			SET_PV_EVENT(IS_SENT_REREG, now + 1000, 0);
			return;

		case IS_SENT_REREG:
			xdputs("Sending OLQ");
			sendPacket(INV_SRC, 0, CC_REG, FC_OLQ, 0, NULL);
			/* Expect OLQ within 2 seconds */
			SET_PV_EVENT(IS_SENT_OLQ, now + 2000, 1);
			return;

		case IS_SENT_OLQ:
			xdputs("No OLQR");
			/* Go back to REREG in 5 seconds */
			SET_PV_EVENT(IS_NOT_REG, now + 5000, 0);
			return;

		case IS_SENT_QUERY:
			/* Go back to REREG */
			xdputs("Timeout waiting for data. rereg");
			SET_PV_EVENT(IS_NOT_REG, now, 0);
			return;

		case IS_QUERY_WAIT:
			/* Time to fetch data again */
			sendPacket(INV_SRC, INV_DST, CC_READ, FC_RODATA, 0, NULL);
			SET_PV_EVENT(IS_SENT_QUERY, now + 2000, 1);
			return;
	}
}

/* Debounce the ground fault button */
static byte checkFaultButton()
{
#if 0
	byte state1, state2;

	state1 = digitalRead(GROUND_FAULT_BUTTON);
	do {
		delay(50);
		state2 = state1;
		state1 = digitalRead(GROUND_FAULT_BUTTON);
	} while (state1 != state2);

	return state1 == LOW;
#else
	/* For now, don't bother debouncing */
	return digitalRead(GROUND_FAULT_BUTTON) == LOW;
#endif
}

/* During the system loop we:
 * Check if uptime is > 1 day and reset
 * Check if no pvoutput update for 60 minutes and reset
 * Process cli input
 * Hit the watchdog
 * Reset the local alarm if the button is pressed
 */
static void system_loop()
{
	unsigned long now = millis();

	if (now > 24 * ONE_HOUR) {
		xputs("Up for 1 day, reset...");
		delay(1000);
		reset();
		return;
	}
	if (now - last_pvupdate >= ONE_HOUR) {
		/* 60 minutes unable to update PV Output, so try rebooting */
		/* Note that we specifically choose 60 minutes here to re-trip any ground alarm */
		xputs("No net update for 1hr, reset...");
		delay(1000);
		reset();
		return;
	}

#if CLI_CONFIG
	check_cli();
#endif
	wdt_reset();

	/* Drive the ground fault alarm appropriately */
	if (ground_error) {
		if (checkFaultButton()) {
			/* Turn off the alarm for 60 minutes */
			next_ground_err_check = now + ONE_HOUR;
		}
		else if (now >= next_ground_err_check) {
			digitalWrite(GROUND_FAULT_PIN, HIGH);
			return;
		}
	}
	digitalWrite(GROUND_FAULT_PIN, LOW);
}

static int net_loop_callback()
{
	system_loop();
	pv_loop();
}

/* This loop maintains the network state.
 * i.e. getting an IP address
 */
static void net_loop()
{
	if (!network_initialised) {
		if ((econfig.dhcp && !FORCE_STATIC) || FORCE_DHCP) {
			xputs("Wait for DHCP...");
			if (!Ethernet.begin(econfig.mac, net_loop_callback)) {
				xputs("No DHCP");
				return;
			}
		}
		else {
			IPAddress gw(econfig.gw);
			if (gw == INADDR_NONE) {
				/* If no gw, we probably can't get to google for DNS */
				Ethernet.begin(econfig.mac, IPAddress(econfig.ip));
			}
			else {
				Ethernet.begin(econfig.mac, IPAddress(econfig.ip), /* google dns */ IPAddress(8,8,8,8), gw);
			}
		}

		ntpc.begin(8888);

		fdev_setup_stream(&pvcfh, pvc_putter, NULL, _FDEV_SETUP_WRITE);
#if TELNET_SERVER && CLI_CONFIG
		ts.begin();
		fdev_setup_stream(&telnetfh, telnet_putter, NULL, _FDEV_SETUP_WRITE);
#endif
		network_initialised = 1;

		/* Inidicate that the network is initialised */
		digitalWrite(LED_PIN, HIGH);
		/* Now is a good time to get a random value */
		randomSeed(analogRead(0) ^ millis());
	}
}

/* This loop maintains the NTP request
 */
static void ntp_loop()
{
	if (epoch == 0 && network_initialised) {
		unsigned long now = millis();
		if (now >= next_ntp) {
			sendNTPpacket(ntpc);
			next_ntp = now + 30000;
		}
		else if (ntpc.parsePacket()) {
			/* Adjust epoch for boot time */
			parseNTPpacket(ntpc);
			if (epoch) {
				send_immediately = 1;
			}
		}
	}
}

void loop() {
	system_loop();
	net_loop();
	ntp_loop();
	pv_loop();
}
